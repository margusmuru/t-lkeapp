/**
 * Created by Margus Muru on 06.06.2017.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WordPair, DictionaryService } from '../word.srv';

@Component({
    selector: 'dictionary',
    templateUrl: 'app/dictionary/dictionary.html'
})

export class DictionaryComponent{
    inputText: string;
    result: string = '';
    estToEng: boolean = true;

    wordsList: WordPair[];

    constructor(public dicService: DictionaryService){}

    ngOnInit(): void{
        this.result = 'tulemus';
        this.getWords();
    }

    getWords(){
        this.dicService.getWords().then(words =>
            this.wordsList = words
        );
    }

    translate(): void{
        this.result = '';
        for(let el of this.wordsList){
            if(this.estToEng){
                if(this.inputText.trim().toLowerCase() === el.est){
                    this.result = el.eng;
                    break;
                }
            }else{
                if(this.inputText.trim().toLowerCase() === el.eng){
                    this.result = el.est;
                    break;
                }
            }
        }
        if(this.result === ''){
            this.result = 'tulemust ei leitud';
        }
    }
    //est-eng == true
    selectLang(lang: boolean){
        if(lang){
            this.estToEng = true;
        }else{
            this.estToEng = false;
        }
    }
}