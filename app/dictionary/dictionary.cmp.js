var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DictionaryService } from '../word.srv';
var DictionaryComponent = (function () {
    function DictionaryComponent(dicService) {
        this.dicService = dicService;
        this.result = '';
        this.estToEng = true;
    }
    DictionaryComponent.prototype.ngOnInit = function () {
        this.result = 'tulemus';
        this.getWords();
    };
    DictionaryComponent.prototype.getWords = function () {
        var _this = this;
        this.dicService.getWords().then(function (words) {
            return _this.wordsList = words;
        });
    };
    DictionaryComponent.prototype.translate = function () {
        this.result = '';
        for (var _i = 0, _a = this.wordsList; _i < _a.length; _i++) {
            var el = _a[_i];
            if (this.estToEng) {
                if (this.inputText.trim().toLowerCase() === el.est) {
                    this.result = el.eng;
                    break;
                }
            }
            else {
                if (this.inputText.trim().toLowerCase() === el.eng) {
                    this.result = el.est;
                    break;
                }
            }
        }
        if (this.result === '') {
            this.result = 'tulemust ei leitud';
        }
    };
    DictionaryComponent.prototype.selectLang = function (lang) {
        if (lang) {
            this.estToEng = true;
        }
        else {
            this.estToEng = false;
        }
    };
    return DictionaryComponent;
}());
DictionaryComponent = __decorate([
    Component({
        selector: 'dictionary',
        templateUrl: 'app/dictionary/dictionary.html'
    }),
    __metadata("design:paramtypes", [DictionaryService])
], DictionaryComponent);
export { DictionaryComponent };
