import { Routes } from '@angular/router';

import {DictionaryComponent} from './dictionary/dictionary.cmp';

export const routes: Routes = [
    { path: 'dictionary', component: DictionaryComponent },
    { path: '', redirectTo: 'dictionary', pathMatch: 'full' }
];

