var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService, Contact } from '../contact.srv';
var NewComponent = (function () {
    function NewComponent(route, router, contactService) {
        this.route = route;
        this.router = router;
        this.contactService = contactService;
        this.update = false;
    }
    NewComponent.prototype.back = function () {
        this.router.navigateByUrl('/search');
    };
    NewComponent.prototype.ngOnInit = function () {
        var _this = this;
        var parsedId = this.route.snapshot.paramMap.get('id');
        if (parsedId != null) {
            this.update = true;
            this.contactService.getContact(parseInt(parsedId))
                .then(function (contact) {
                _this.contact = contact;
                _this.newContactName = contact.name;
                _this.newContactPhone = contact.phone;
            });
        }
    };
    NewComponent.prototype.addNewTask = function () {
        var _this = this;
        if (this.update) {
            this.contact.name = this.newContactName;
            this.contact.phone = this.newContactPhone;
            this.contactService.updateContacts(this.contact)
                .then(function () {
                _this.newContactName = '';
                _this.newContactPhone = '';
                _this.back();
            });
        }
        else {
            this.contactService.saveContacts(new Contact(this.newContactName, this.newContactPhone))
                .then(function () {
                _this.newContactName = '';
                _this.newContactPhone = '';
                _this.back();
            });
        }
    };
    return NewComponent;
}());
NewComponent = __decorate([
    Component({
        selector: 'new',
        templateUrl: 'app/new/new.html'
    }),
    __metadata("design:paramtypes", [ActivatedRoute,
        Router,
        ContactService])
], NewComponent);
export { NewComponent };
