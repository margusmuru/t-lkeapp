import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ContactService, Contact } from '../contact.srv';

@Component({
    selector: 'new',
    templateUrl: 'app/new/new.html'
})
export class NewComponent implements OnInit {

    contact: Contact;
    update: boolean = false;
    newContactName: string;
    newContactPhone: string;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactService: ContactService) {}

    back() {
        this.router.navigateByUrl('/search');
    }

    ngOnInit(): void {
        var parsedId = this.route.snapshot.paramMap.get('id');

        if(parsedId != null){
            this.update = true;
            this.contactService.getContact(parseInt(parsedId))
                .then(contact => {
                    this.contact = contact;
                    this.newContactName = contact.name;
                    this.newContactPhone = contact.phone;
                });
        }
    }

    addNewTask(): void {

        if(this.update){

            this.contact.name = this.newContactName;
            this.contact.phone = this.newContactPhone;

            this.contactService.updateContacts(this.contact)
                .then(() => {
                    this.newContactName = '';
                    this.newContactPhone = '';
                    this.back();
                });

        }else{

            this.contactService.saveContacts(new Contact(this.newContactName, this.newContactPhone))
                .then(() => {
                    this.newContactName = '';
                    this.newContactPhone = '';
                    this.back();
                });

        }

    }

}
