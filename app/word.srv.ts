/**
 * Created by Margus Muru on 06.06.2017.
 */
import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";

export class WordPair{
    _id: number;
    constructor(public est: string, public eng: string){}
}

@Injectable()
export class DictionaryService{
    constructor(private http: Http) {}

    getWords(): Promise<WordPair[]> {
        return this.http
            .get('api/words/')
            .toPromise()
            .then((response: Response) => response.json());
    }

}