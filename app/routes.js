import { DictionaryComponent } from './dictionary/dictionary.cmp';
export var routes = [
    { path: 'dictionary', component: DictionaryComponent },
    { path: '', redirectTo: 'dictionary', pathMatch: 'full' }
];
