import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";

export class Contact {
    _id: number;
    done: boolean = false;

    constructor(public name : string, public phone : string) {
    };
}

@Injectable()
export class ContactService {
    constructor(private http: Http) {}

    getContacts(): Promise<Contact[]> {
        return this.http
            .get('api/contacts')
            .toPromise()
            .then((response: Response) => response.json());
    }

    getContact(id: number): Promise<Contact> {
        return this.http
            .get('api/contacts/' + id)
            .toPromise()
            .then((response: Response) => response.json());
    }

    saveContacts(contact: Contact): Promise<void> {
        return this.http
            .post('api/contacts', contact)
            .toPromise()
            .then(() => <void>null);
    }

    updateContacts(contact: Contact): Promise<void> {
        return this.http
            .put('api/contacts/' + contact._id, contact)
            .toPromise()
            .then(() => <void>null);
    }

    deleteContact(id: number): Promise<void> {
        return this.http
            .delete('api/contacts/' + id)
            .toPromise()
            .then(() => <void>null);
    }

}