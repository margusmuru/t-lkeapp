var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
var Contact = (function () {
    function Contact(name, phone) {
        this.name = name;
        this.phone = phone;
        this.done = false;
    }
    ;
    return Contact;
}());
export { Contact };
var ContactService = (function () {
    function ContactService(http) {
        this.http = http;
    }
    ContactService.prototype.getContacts = function () {
        return this.http
            .get('api/contacts')
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ContactService.prototype.getContact = function (id) {
        return this.http
            .get('api/contacts/' + id)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ContactService.prototype.saveContacts = function (contact) {
        return this.http
            .post('api/contacts', contact)
            .toPromise()
            .then(function () { return null; });
    };
    ContactService.prototype.updateContacts = function (contact) {
        return this.http
            .put('api/contacts/' + contact._id, contact)
            .toPromise()
            .then(function () { return null; });
    };
    ContactService.prototype.deleteContact = function (id) {
        return this.http
            .delete('api/contacts/' + id)
            .toPromise()
            .then(function () { return null; });
    };
    return ContactService;
}());
ContactService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], ContactService);
export { ContactService };
