import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.cmp';
import {DictionaryService } from './word.srv';
import {RouterModule} from '@angular/router';
import {DictionaryComponent} from './dictionary/dictionary.cmp';
import {routes} from './routes';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot(routes, {useHash: true})
    ],
    declarations: [ AppComponent, DictionaryComponent ],
    providers: [DictionaryService],
    bootstrap: [ AppComponent ]
})
export class AppModule { }