var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ContactService } from '../contact.srv';
var SearchComponent = (function () {
    function SearchComponent(contactService) {
        this.contactService = contactService;
        this.contacts = [];
    }
    SearchComponent.prototype.updateContacts = function () {
        var _this = this;
        this.contactService.getContacts().then(function (contacts) { return _this.contacts = contacts; });
    };
    SearchComponent.prototype.deleteContact = function (contactId) {
        var _this = this;
        this.contactService.deleteContact(contactId)
            .then(function () { return _this.updateContacts(); });
    };
    SearchComponent.prototype.deleteSelected = function () {
        var _this = this;
        for (var i = 0; i < this.contacts.length; i++) {
            if (this.contacts[i].done) {
                this.contactService.deleteContact(this.contacts[i]._id)
                    .then(function () { return _this.updateContacts(); });
            }
        }
    };
    SearchComponent.prototype.selectAll = function () {
        for (var i = 0; i < this.contacts.length; i++) {
            this.contacts[i].done = true;
        }
    };
    SearchComponent.prototype.ngOnInit = function () {
        this.updateContacts();
    };
    return SearchComponent;
}());
SearchComponent = __decorate([
    Component({
        selector: 'search',
        templateUrl: 'app/search/search.html',
        styleUrls: ['app/search/search.css']
    }),
    __metadata("design:paramtypes", [ContactService])
], SearchComponent);
export { SearchComponent };
