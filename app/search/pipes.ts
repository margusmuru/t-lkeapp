import {Pipe, PipeTransform} from '@angular/core';
import {Contact} from '../contact.srv';

/**
 * Created by Margus Muru on 31.05.2017.
 */

@Pipe({
    name: 'FilterPipe',
})
export class FilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            input = input.toString().toLowerCase();
            return value.filter(function (el: Contact) {
                var name = el.name.toString();
                if(el.phone != null){
                    name = name + el.phone.toString();
                }
                return name.toLowerCase().indexOf(input) > -1;
            })
        }
        return value;
    }
}