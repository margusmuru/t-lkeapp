import { Component, OnInit } from '@angular/core';
import { Contact, ContactService } from '../contact.srv';
import {FilterPipe} from './pipes'

@Component({
    selector: 'search',
    templateUrl: 'app/search/search.html',
    styleUrls: ['app/search/search.css']
})
export class SearchComponent implements OnInit {

    contacts: Contact[] = [];


    constructor(private contactService: ContactService) {}

    private updateContacts(): void {
        this.contactService.getContacts().then(contacts => this.contacts = contacts);
    }


    deleteContact(contactId : number): void {
        this.contactService.deleteContact(contactId)
            .then(() => this.updateContacts());
    }

    deleteSelected(): void {
        for(var i = 0; i < this.contacts.length; i++){
            if(this.contacts[i].done){
                this.contactService.deleteContact(this.contacts[i]._id)
                    .then(() => this.updateContacts());
            }
        }
    }

    selectAll(): void{
        for(var i = 0; i < this.contacts.length; i++){
            this.contacts[i].done = true;
        }
    }

    ngOnInit(): void {
        this.updateContacts();
    }

}




